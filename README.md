For example, the following query:

select avg(`CountLineCode`)
from tomcatall
where `Kind`='Public Class'

Gives us the value 136.5, meaning that the average lines of code for all the Public classes in more than 100 versions of tomcat is 136.5.

Then, if I wanted to check the number of God classes, let’s say classes with more than 300 LOC:

select count(*)
from tomcatall
where `CountLineCode`>300 and `Kind`='Public Class' 

We obtain the number 14252.

Considering that the total number of Public Classes in our corpus is 122276, according to our definition of God Class, the 11.6% of the classes are God Classes.




_________________________________________________


We have 113 tomcat versions in our database:

SELECT count(DISTINCT substring_index(substring_index(File, '/Tomcat/', -1),'/', 1))
FROM tomcatall

And to obtain the list of the versions, just use this query:

SELECT DISTINCT substring_index(substring_index(File, '/Tomcat/', -1),'/', 1)
FROM tomcatall






Number of Public Class per version:


SELECT count(kind) as NumberOfPC, substring_index(substring_index(File, '/Tomcat/', -1),'/', 1) as Version
FROM tomcatall
where kind='Public Class'
group by version