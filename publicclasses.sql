# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.46)
# Database: tomcat
# Generation Time: 2016-09-10 14:57:24 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table publicclasses
# ------------------------------------------------------------

DROP VIEW IF EXISTS `publicclasses`;

CREATE TABLE `publicclasses` (
   `Kind` TEXT NULL DEFAULT NULL,
   `Name` TEXT NULL DEFAULT NULL,
   `File` TEXT NULL DEFAULT NULL,
   `AvgCyclomatic` INT(11) NULL DEFAULT NULL,
   `AvgCyclomaticModified` INT(11) NULL DEFAULT NULL,
   `AvgCyclomaticStrict` INT(11) NULL DEFAULT NULL,
   `AvgEssential` INT(11) NULL DEFAULT NULL,
   `AvgLine` INT(11) NULL DEFAULT NULL,
   `AvgLineBlank` INT(11) NULL DEFAULT NULL,
   `AvgLineCode` INT(11) NULL DEFAULT NULL,
   `AvgLineComment` INT(11) NULL DEFAULT NULL,
   `CountClassBase` TEXT NULL DEFAULT NULL,
   `CountClassCoupled` TEXT NULL DEFAULT NULL,
   `CountClassDerived` TEXT NULL DEFAULT NULL,
   `CountDeclClass` INT(11) NULL DEFAULT NULL,
   `CountDeclClassMethod` INT(11) NULL DEFAULT NULL,
   `CountDeclClassVariable` INT(11) NULL DEFAULT NULL,
   `CountDeclFile` TEXT NULL DEFAULT NULL,
   `CountDeclFunction` TEXT NULL DEFAULT NULL,
   `CountDeclInstanceMethod` INT(11) NULL DEFAULT NULL,
   `CountDeclInstanceVariable` INT(11) NULL DEFAULT NULL,
   `CountDeclMethod` INT(11) NULL DEFAULT NULL,
   `CountDeclMethodAll` TEXT NULL DEFAULT NULL,
   `CountDeclMethodDefault` INT(11) NULL DEFAULT NULL,
   `CountDeclMethodPrivate` INT(11) NULL DEFAULT NULL,
   `CountDeclMethodProtected` INT(11) NULL DEFAULT NULL,
   `CountDeclMethodPublic` INT(11) NULL DEFAULT NULL,
   `CountInput` TEXT NULL DEFAULT NULL,
   `CountLine` INT(11) NULL DEFAULT NULL,
   `CountLineBlank` INT(11) NULL DEFAULT NULL,
   `CountLineCode` INT(11) NULL DEFAULT NULL,
   `CountLineCodeDecl` INT(11) NULL DEFAULT NULL,
   `CountLineCodeExe` INT(11) NULL DEFAULT NULL,
   `CountLineComment` INT(11) NULL DEFAULT NULL,
   `CountOutput` TEXT NULL DEFAULT NULL,
   `CountPath` TEXT NULL DEFAULT NULL,
   `CountSemicolon` INT(11) NULL DEFAULT NULL,
   `CountStmt` INT(11) NULL DEFAULT NULL,
   `CountStmtDecl` INT(11) NULL DEFAULT NULL,
   `CountStmtExe` INT(11) NULL DEFAULT NULL,
   `Cyclomatic` TEXT NULL DEFAULT NULL,
   `CyclomaticModified` TEXT NULL DEFAULT NULL,
   `CyclomaticStrict` TEXT NULL DEFAULT NULL,
   `Essential` TEXT NULL DEFAULT NULL,
   `MaxCyclomatic` INT(11) NULL DEFAULT NULL,
   `MaxCyclomaticModified` INT(11) NULL DEFAULT NULL,
   `MaxCyclomaticStrict` INT(11) NULL DEFAULT NULL,
   `MaxEssential` INT(11) NULL DEFAULT NULL,
   `MaxInheritanceTree` TEXT NULL DEFAULT NULL,
   `MaxNesting` INT(11) NULL DEFAULT NULL,
   `PercentLackOfCohesion` TEXT NULL DEFAULT NULL,
   `RatioCommentToCode` DOUBLE NULL DEFAULT NULL,
   `SumCyclomatic` INT(11) NULL DEFAULT NULL,
   `SumCyclomaticModified` INT(11) NULL DEFAULT NULL,
   `SumCyclomaticStrict` INT(11) NULL DEFAULT NULL,
   `SumEssential` INT(11) NULL DEFAULT NULL,
   `Version` TEXT NULL DEFAULT NULL
) ENGINE=MyISAM;





# Replace placeholder table for publicclasses with correct view syntax
# ------------------------------------------------------------

DROP TABLE `publicclasses`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `publicclasses`
AS SELECT
   `tomcatall`.`Kind` AS `Kind`,
   `tomcatall`.`Name` AS `Name`,
   `tomcatall`.`File` AS `File`,
   `tomcatall`.`AvgCyclomatic` AS `AvgCyclomatic`,
   `tomcatall`.`AvgCyclomaticModified` AS `AvgCyclomaticModified`,
   `tomcatall`.`AvgCyclomaticStrict` AS `AvgCyclomaticStrict`,
   `tomcatall`.`AvgEssential` AS `AvgEssential`,
   `tomcatall`.`AvgLine` AS `AvgLine`,
   `tomcatall`.`AvgLineBlank` AS `AvgLineBlank`,
   `tomcatall`.`AvgLineCode` AS `AvgLineCode`,
   `tomcatall`.`AvgLineComment` AS `AvgLineComment`,
   `tomcatall`.`CountClassBase` AS `CountClassBase`,
   `tomcatall`.`CountClassCoupled` AS `CountClassCoupled`,
   `tomcatall`.`CountClassDerived` AS `CountClassDerived`,
   `tomcatall`.`CountDeclClass` AS `CountDeclClass`,
   `tomcatall`.`CountDeclClassMethod` AS `CountDeclClassMethod`,
   `tomcatall`.`CountDeclClassVariable` AS `CountDeclClassVariable`,
   `tomcatall`.`CountDeclFile` AS `CountDeclFile`,
   `tomcatall`.`CountDeclFunction` AS `CountDeclFunction`,
   `tomcatall`.`CountDeclInstanceMethod` AS `CountDeclInstanceMethod`,
   `tomcatall`.`CountDeclInstanceVariable` AS `CountDeclInstanceVariable`,
   `tomcatall`.`CountDeclMethod` AS `CountDeclMethod`,
   `tomcatall`.`CountDeclMethodAll` AS `CountDeclMethodAll`,
   `tomcatall`.`CountDeclMethodDefault` AS `CountDeclMethodDefault`,
   `tomcatall`.`CountDeclMethodPrivate` AS `CountDeclMethodPrivate`,
   `tomcatall`.`CountDeclMethodProtected` AS `CountDeclMethodProtected`,
   `tomcatall`.`CountDeclMethodPublic` AS `CountDeclMethodPublic`,
   `tomcatall`.`CountInput` AS `CountInput`,
   `tomcatall`.`CountLine` AS `CountLine`,
   `tomcatall`.`CountLineBlank` AS `CountLineBlank`,
   `tomcatall`.`CountLineCode` AS `CountLineCode`,
   `tomcatall`.`CountLineCodeDecl` AS `CountLineCodeDecl`,
   `tomcatall`.`CountLineCodeExe` AS `CountLineCodeExe`,
   `tomcatall`.`CountLineComment` AS `CountLineComment`,
   `tomcatall`.`CountOutput` AS `CountOutput`,
   `tomcatall`.`CountPath` AS `CountPath`,
   `tomcatall`.`CountSemicolon` AS `CountSemicolon`,
   `tomcatall`.`CountStmt` AS `CountStmt`,
   `tomcatall`.`CountStmtDecl` AS `CountStmtDecl`,
   `tomcatall`.`CountStmtExe` AS `CountStmtExe`,
   `tomcatall`.`Cyclomatic` AS `Cyclomatic`,
   `tomcatall`.`CyclomaticModified` AS `CyclomaticModified`,
   `tomcatall`.`CyclomaticStrict` AS `CyclomaticStrict`,
   `tomcatall`.`Essential` AS `Essential`,
   `tomcatall`.`MaxCyclomatic` AS `MaxCyclomatic`,
   `tomcatall`.`MaxCyclomaticModified` AS `MaxCyclomaticModified`,
   `tomcatall`.`MaxCyclomaticStrict` AS `MaxCyclomaticStrict`,
   `tomcatall`.`MaxEssential` AS `MaxEssential`,
   `tomcatall`.`MaxInheritanceTree` AS `MaxInheritanceTree`,
   `tomcatall`.`MaxNesting` AS `MaxNesting`,
   `tomcatall`.`PercentLackOfCohesion` AS `PercentLackOfCohesion`,
   `tomcatall`.`RatioCommentToCode` AS `RatioCommentToCode`,
   `tomcatall`.`SumCyclomatic` AS `SumCyclomatic`,
   `tomcatall`.`SumCyclomaticModified` AS `SumCyclomaticModified`,
   `tomcatall`.`SumCyclomaticStrict` AS `SumCyclomaticStrict`,
   `tomcatall`.`SumEssential` AS `SumEssential`,substring_index(substring_index(`tomcatall`.`File`,'/Tomcat/',-(1)),'/',1) AS `Version`
FROM `tomcatall` where (`tomcatall`.`Kind` = 'Public Class');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
